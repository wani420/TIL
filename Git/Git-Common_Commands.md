# Git - Common Commands

|Description|Commands|
|-----------|--------|
|Cancel a rebasing|`git rebase --abort`|
|Push tags to remote|`git push --tags`|
|Delete a local tag|`git tag -d <tag_name>`| 
|Delete a remote tag|`git push --delete origin <tag_name>`|
|Remove all untracked files|`git clean -fd`|
|Reset to last commit|`git reset --hard HEAD~1`|