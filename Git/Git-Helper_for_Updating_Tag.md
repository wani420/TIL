# Git - Helper for Updating Tag

Write a shell function in `bash profile`

```
gitUpdateTag() {
    git tag -d $1
    git push origin --delete $1
    git tag $1
    git push --tags
}

alias git_update_tag=gitUpdateTag
```

Update a tag with command `git_update_tag`

```
git_update_tag 1.0.0
```