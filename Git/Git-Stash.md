# Git - Stash

## Stash Specifi Files

1. Use `git add` to add those files you don't want to stash.
2. `git stash --keep-index`.
3. `git reset` if needed to reset the added index files.