# Git - Remove Submodule

**Step 1. Deinit**

```
$ git deinit <submodule_name>
```

**Step 2. Remove Submodule Files**
This step will also delete the submodule settings in `.gitmodules`

```
$ git rm <submodule_path>
```

**Step 3. Remove files in `.git/modules`**

This step is not very intuitive, but you still have to do it.
Check the `modules` directory in `.git` diretory, and remove the diretory tree which is corresponding
to the path of the submodule in the prohect.

```
$ rm -rf .git/modules/<submodule_path>
```