# Git - Add Command Aliases

Set by command `git config --global`

```
git config --global alias.c commit
git config --global alias.co checkout
git config --global alias.st status
git config --global alias.br branch
git conifg --global alias.clear reset --hard 
```

Or Edit `~/.gitconfig` directly

```
[alias]
      c = commit
      clear = reset --hard
      co = checkout
      cb = checkout -b
      st = status
      br = branch
```