# iOS - DispatchSemaphore vs DispatchGroup

## When to use them?

When we want to wait asynchrounous operations.   
But the mechanism is differnt.
DispatchGroup is like, `tell me when all of these are finished`.
DispatchSemaphore is like, `wait right here until all of the previous tasks are finished`.   
So DispatchSemaphore will block the thread until all the things are done.

## Example of DispatchGroup

```
let dispatchGroup = DispatchGroup()

dispatchGroup.enter()
longRunningFunction { dispatchGroup.leave() }

dispatchGroup.enter()
longRunningFunctionTwo { dispatchGroup.leave() }

dispatchGroup.notify(queue: .main) {
    print("Both functions complete 👍")
}
```

## Example of DispatchSemaphore

The value in DispatchSemaphore need to be clearly understanded.
You create a DispatchSemaphore with non-negative values, usually is 0.
When you call `wait`, the value will minus 1, and the thread will be blocked
when the value is less than 0.
And you call `signal` when you finished your task, which will plus the value by 1.


**The most important thing is, you need to make sure `wait` and `signal` are paired. 
If the semaphore object is disposed with `value less than its initial value`, the `app crashes`.**

```
func getResult() -> Result? {
    let semaphore = DispatchSemaphore(value: 0)
    
    var result:Result?
    
    getResultAsync() { r in
        result = r
        semaphore.signal()
    }
    
    // thread blocked here until signal is called
    semaphore.wait()
    
    return result
}

```
