# iOS - Testing Frameworks

## BDD + Matcher

- Quick + Nimble
- Specta + Expecta
- Kiwi
- Cedar

## Mathcer

- OCHamcrest

## Mock & Stub

- OCMock
- OCMockito
- OHHTTPStubs

## UI

- KIF
- FBSnapshotTestCase
- EarlGrey
