# iOS - Universal Link

## Step 1. iOS Developer Cetner

Go to the `App IDs` section and edit the app ID which you want to enable `Universal Link`,
and enable the `Associate Domain` by clicking the checkbox.

![](http://i.imgur.com/2nthxBG.png)

After updating the app ID setting you will also need to `regenerate` and `download` the `Provisioing Profile` you used
for code signing your app.

## Step 2. Setup Server

In the domain URL you want to enable `Uiniversal Link`, you need to add a file named `apple-app-site-association` in your root path. 
When you go to the URL `yourdomain.com/apple-app-site-association` should be able to see something like the following content.

```
{
    "applinks": {
        "apps": [],
        "details": [
            {
                "appID": "1424KMT6KJ.com.company.Product",
                "paths": [ "/" , "/some_path" ]
            }
        ]
    }
}
```

In the `details` you can specify which Apps you're going to support the `Universal Link`. 

The `appId` is composed of `app ID prefix` and the `bundle ID` of your app.
You can find the `app ID prefix` in the iOS Developer Center, in the App IDs section.
The `paths` is the paths on your website which you would like to support the `Universal Link`.

## Step 3. Setup Client App

(1) In the `Capablities` tab of Project Settings, enable the `Associated Domains` and add the domain of your server
with prefix `applinks:` like `applinks:mydomain.com`.

![](http://i.imgur.com/IkN3VCZ.png)

(2) In `AppDelegate.m`, override `application:continueUserActivity:restorationHandler:` and add some custom actions.

```
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler {
    
    if ([userActivity.webpageURL.host isEqualToString:@"mydomain.com"] &&
        [userActivity.webpageURL.path isEqualToString:@"/some_path"]) {
        // Do Something
    }
    
    return YES;
}
```

## Step 4. Test

After you've done all the settings and install the app on your device. Go to `mydomain.com` on safari and scroll down 
you should see a banner shows up with your app name and a `Open` button.
If you click the link in other apps like `Slack`, your app should be opended directly.


