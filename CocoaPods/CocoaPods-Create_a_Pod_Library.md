# CocoaPods - Create a Pod Library

##### (1) Run `pod lib create <your_lib_name>` in terminal

This will ask you couple of questions, just answer them one by one.

##### (2) Write your code in `Development Pods` of Pods Target

![](http://i.imgur.com/NjG5I1xm.png)