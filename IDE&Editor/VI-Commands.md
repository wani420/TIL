# VI - Commands

|Description|command|
|---|---|
|Delete Line|`D` + `D`|
|Duplicate Line|`Y` + `Y` + `P` OR `Shift` + `Y` + `P`| 
|Insert at Line End|`Shift` + `A`|
|Go to File End|`Shift` + `G`|
|Go to Line Head|`0`|
|Go to Line End|`Shift` + `$`|
|Undo|`U`|
|Redo|`Ctrl` + `R`|
