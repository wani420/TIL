# JetBrains IDEs - Shortcuts

|Description|Shortcut|
|---|---|
|Go to method|`Fn` + `Control` + `F12`|
|Find method usage|`Fn` + `Option` + `F7`|
|Preference setting|`Command` + `,`|
