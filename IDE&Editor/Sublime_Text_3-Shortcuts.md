# Sublime Text 3 - Shortcuts

|Description|Shortcut|
|---|---|
|Delete line|`Shift` + `Ctrl` + `K`|
|Duplicate line|`Shift` + `Command` + `D`|
|1 column Layout|`Option` + `Command` + `1`|
|2 column Layout|`Option` + `Command` + `2`|
|3 column Layout|`Option` + `Command` + `2`|
|2 row Layout|`Shift` + `Option` + `Command` + `2`|
