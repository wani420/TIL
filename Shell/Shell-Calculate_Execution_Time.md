# Shell - Calculate Execution Time

When using MacOS, install `coreutils` first if `gdate` command is not found.

```
brew install coreutils
```

Caculate the execution time time.

```shell
start_time=$(($(gdate +%s%N)/1000000))

#...some execution

end_time=$(($(gdate +%s%N)/1000000))

echo execution time was `expr $end_time - $start_time` ms.
```
