# WWDC 2016 - Visual Debugging with Xcode
 
## Runtime issues

- Thread issues, by using **thread sanitizer**
- UI Layout issues
- Memory issues

## Thread Sanitizer

- Data races
- Use of uninitialized mutexes
- Unlock from wrong thread
- Thread leaks
- Unsafe calls in signal handlers

> Dig more by watching `Thread Sanitizer` and `Static Analysis` sessions.

## View Debugging

- 70% faster in screenshots
- More acurrate in rendering
- Report ambiguos layout issues

## FPS Perfomance Gauge

- Shows FPS
- Shows CPU and GPU usage, so can tell whether it's CPU-bound or GPU-bound
- Timeline history

## Quick Look

- Visualize entire state machine

## Memory Graph Debugger

- Enable malloc logging in scheme, set logging option to `Live applications only` to reduce unnecessary info. Malloc scribble can also be enabled to improve acurracy.

![](enable_malloc_logging.png)

- In `Debug Navigator`, check memory hierachy by clicking the `View process in different ways` button.

![](memory_hierachy_buttton.png)

- Click an object and use inspector to see more details, which can check address, class, hierachy and backtrace.

![](memory_hierachy_inspect.png)

- !! Memory graph debugger requires to turn off sanitizers like `Addrrss Sanitizer` and `Thread Sanitizer`.