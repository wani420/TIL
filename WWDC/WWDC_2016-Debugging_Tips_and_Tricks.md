# WWDC 2016 - Debugging Tips and Tricks

## Console Enhancement

- Open console in terminal (Only avaliable on MacOS developement)

## Swift REPL

- It's actually a LLDB
- Start REPL, type `swift` in terminal
- Prefix with `:`, can use LLDB command directly

Use type **lookup** to display content inside specific class, module, or function

```
:type lookup Class
:type lookup func_name
```

Use `:b line_number` to add breakpoint, for example

```
1. func greet() {
2.  print("hello")
3. }

> :b 2
> greet()
```

The LLDB will show up because it stops at the breakpoint.

Enter : switch to `LLDB`, enter `repl` to switch back.

```
(repl) :
(lldb) repl
(repl)
```

## LLDB

Under some circumstances, might want to automate debugging tasks, you can run LLDB commands insde a file

```
lldb --source <filename>
```

Run LLDB command without providing a file

```
lldb --one-line command
```

Use frame to print object to prevent unexpected extra excution

```
fr v <variable_name>
```
