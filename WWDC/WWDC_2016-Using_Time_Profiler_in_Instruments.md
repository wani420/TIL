# WWDC 2016 - Using Time Profiler in Instruments

## Concepts

- Time profiling is not an actual measurement of duration, it's the number of samples multiplied by the time between samples, which aggregates the samples into a useful summary.
- For it's not an actual measurement, the side effect is it can't distinguish between long running methods or much faster methods that called repetitively.
- Method A run 1 minute and runs one time, method B run 10 seconds and runs ten times, may look the sample on time profiler?
- Really fast functions will also not appear on time profiler.


## Basic Operations

- Use `commnad +` and `command -` to zoom in and out
- Drag the blue line with triangle on top to navigate between different time
- `Option` select a area to zoom in, `Control` select a areat to zoom out.
- `Ctrol + Command + Z` to reset zoom scale.
- Click the area under `Life cycle` to reset selection.
- Right top segments can switch different view mode, to spearate by `CPU cores` or `CPU and state` or `Threads`.

## Look into issues

1. Select a area of heavy CPU usage to look into what's going on inside.
2. Inspect the the function calls appear in the details console.
3. Expand the function call with `heavy weight`, **hold option to click the triangle** for a smart expansion of the call stack, it will auto expand until there is intersting data.

## Best Practices

- Run with release builds
- Profile on devices not simulator
- Profile on old devices
- Use large data sets where it makes sense
- Look for poorly scaling code, Ex O(N^2)

## Related Sessions

- Optimizing App Startup Time
- System Trace in Depth
- Thread Sanitizer and Static Analysis
- Optimizing I/O for Performance and Battery Life
- Unified Logging and Activity Tracing
- **Profiling in Depth** (More detail in time profiler)