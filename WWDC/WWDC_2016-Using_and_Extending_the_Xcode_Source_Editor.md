# WWDC 2016 - Using and Extending the Xcode Source Editor

## Useful Commands

### Doumentation comment

Use `Command + Option + /` in front of any class, method, type will auto generate documentation comment.

### Search 

Use `Command + E` on the word you want to search, will automatically paste the word to search bar. And use `Command + G` to jump between search result.

Use `Command + Shift + F` to do whole project find. Use `Ctrl + Command + G` to move to next search result, and use `Shift + Ctrl + Command + G` to move up.

### Move Line

Use `Command + Option + [` and `Command + Option + ]`

## FIXME and TODO Hint

- `// FIXME:` and `// TODO:` will show up on the method navigate menu.

![](fix_me_to_do_hint.png)

## Image Literal and Color Literal (Swift Only)

**A thumbnail of image and color will be displayed infront of the variable for convenience.**

Fisrt go to `Key Bindings` in settings and search `completion` to make sure `Show Completions` is not conflicted with others, `Show Completions` shotcurt will be used later on.

### Image Literal

New a variable and use `Show Completions` shotcurt to type the image name directly.

![](img_literal_1.png)

![](img_literal_2.png)

### Color Literal

New a variable and use `Show Completions` shotcurt to type `Color Literal`.

![](color_literal.png)

## Source Editor Extesnion


