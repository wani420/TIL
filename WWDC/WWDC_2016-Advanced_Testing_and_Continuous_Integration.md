# Advanced Testing and Continuous Integration

## Difference between Unit Test and UI Test

Unit test, it's loaded directly into the application, application play as a `host application`.
UI test, the application is loaded into `test runner` as a `target application`.


## Test Observation

`XCTestObservation` is used to observe test life cycle.
The whole test is a `test bundle`, and contains couples of `test suite` within it. 

Use `testBundleWill..` and `testBundleDid..` to observe test bundle life cycle, use `testSuiteWill..` and `testSuiteDid..` to observe test suite life cycle.

Steps to add 

(1) Create a class and coforms to `XCTestObservation` protocol.

(2) In init method add self to testObservers of `XCTestObservation` shared instance.

```
- (instancetype)init
{
    self = [super init];
    
    if (self) {
        [[XCTestObservationCenter sharedTestObservationCenter]
         addTestObserver:self];
    }
    
    return self;
}
```

(3) In test info plist add `Principal Class` property, set its value to this class. 
