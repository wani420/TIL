# WWDC 2016 - Thread Sanitizer and Static Analysis

## Address Sanitizer

- Introduce in 2015
- Finds memory corrutption issues
- Now has swift support

## Thrading issues

- Hard to consistently reproduce
- Diffcult to debug
- Lead to unpredicable results

## Thread Sanitizer

### Checks

- Use of uninitialized mutexes
- Thread leaks
- Unsafe calls in dignal handler
- Unlock from wrong thread
- Data races

### Caveat

Thread Sanitizer requires using 64 bit simulator, like `iPhone 6S`.   
Not allow on device.

### Enable in Scheme

![](enable_thread_sanitizer.png)

### Benign Race

## Supplementary Notes

**pthread_mutex_init**

**pthread_mutex_loc**

