# Chrome - Shortcuts

|Description|Shortcut|
|---|---|
|Go to address bar|`Command` + `L`|
|New tab|`Command` + `T`|
|Go to left tab|`Option` + `Command` + `←`|
|Go to right tab|`Option` + `Command` + `→`|
|Open link and switch to new tab|`Shift` + `Commnad` + `Click`|
|Open link in new tab|`Command` + `Click`|
