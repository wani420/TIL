# Today I Learned

## Xcode

- [Xcode - Shortcuts](Xcode/Xcode-Shortcuts.md)
- [Xcode - Enable Build Timing](Xcode/Xcode-Enable_Build_Timing.md)
- [Xcode - Clean Derived Data](Xcode/Xcode-Clean_Derived_Data.md)
- [Xcode - Search and Replace with Regular Expression](Xcode/Xcode-Search_and_Replace_with_Regular_Expression.md)

## Tools

- [Chrome - Shortcuts](Tools/Chrome-Shortcuts.md)
- [Mac - Terminal Shortcuts](Tools/Mac-Terminal_Shortcuts.md)

## Shell

- [Shell - Calculate Execution Time](Shell/Shell-Calculate_Execution_Time.md)

## LLDB

- [LLDB - Breakpoints](LLDB/LLDB-Breakpoints.md)
- [LLDB - Basic Operations](LLDB/LLDB-Basic_Operations.md)

## iOS

- [iOS - Universal Link](iOS/iOS-Universal_Link.md)
- [iOS - Testing Frameworks](iOS/iOS-Testing_Frameworks.md)
- [iOS - DispatchSemaphore vs DispatchGroup](iOS/iOS-DispatchSemaphore_vs_DispatchGroup.md)

## IDE & Editor

- [VI - Commands](IDE&Editor/VI-Commands.md)
- [Sublime Text 3 - Shortcuts](IDE&Editor/Sublime_Text_3-Shortcuts.md)
- [JetBrains IDEs - Shortcuts](IDE&Editor/JetBrains_IDEs-Shortcuts.md)

## WWDC

- [WWDC 2016 - Thread Sanitizer and Static Analysis](WWDC/WWDC_2016-Thread_Sanitizer_and_Static_Analysis.md)
- [WWDC 2016 - Visual Debugging with Xcode](WWDC/WWDC_2016-Visual_Debugging_with_Xcode.md)
- [WWDC 2016 - Using Time Profiler in Instruments](WWDC/WWDC_2016-Using_Time_Profiler_in_Instruments.md)
- [WWDC 2016 - Using and Extending the Xcode Source Editor](WWDC/WWDC_2016-Using_and_Extending_the_Xcode_Source_Editor.md)
- [WWDC 2016 - Advanced Testing and Continuous Integration](WWDC/WWDC_2016-Advanced_Testing_and_Continuous_Integration.md)
- [WWDC 2016 - Debugging Tips and Tricks](WWDC/WWDC_2016-Debugging_Tips_and_Tricks.md)

## CocoaPods

- [CocoaPods - Commands](CocoaPods/CocoaPods-Commands.md)
- [CocoaPods - Create a Pod Library](CocoaPods/CocoaPods-Create_a_Pod_Library.md)

## Git

- [Git - Stash](Git/Git-Stash.md)
- [Git - Common Commands](Git/Git-Common_Commands.md)
- [Git - Add Command Aliases](Git/Git-Add_Command_Aliases.md)
- [Git - Helper for Updating Tag](Git/Git-Helper_for_Updating_Tag.md)
- [Git - Remove Submodule](Git/Git-Remove_Submodule.md)