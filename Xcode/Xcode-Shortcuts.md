# Xcode - Shortcuts

## Display

- Fold all functions `Shift` + `Option` + `Command` + `←` 
- Unfold all functions `Shift` + `Option` + `Command` + `→` 
- Close assistant editor `Command` + `Enter` 
- Open assistant editor `Command` + `Option` + `Enter` 

## Navigation

- Navigate methods `Control` + `6` 
- Navigate between header and implementation Ffles `Control` + `Command` + `↑` 
- Open file  `Shift` + `Command` + `O` 
- Open file in other editor pane  `Shift` + `Command` + `O` -> `Shift` + `Command` + `Enter` 
- Open file in assistant edior  `Option` + `Click File` 
- Show file position in project navigator `Shift` + `Command` + `J` 

## Source Control

- Show version control `Option` + `Command` + `C` 

## Debuger

- Next break point `Control` + `Command` + `Y` 
- Disable or enable break point `Command` + `Y` 
- Clean console log  `Command` + `K` 

## Documentation

- Add document comment, put cursor infront of method name and `Command` + `Option` + `L`
- Quick Help  `Option` + `Click Symbol` 

## Testing

- Run unit test `Ctrl` + `U` 
- Run unit test on on current method `Ctrl` + `Command` + `U` 
- Run last test `Control + Command + G`

