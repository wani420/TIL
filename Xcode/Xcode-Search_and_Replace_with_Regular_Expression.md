# Xcode - Search and Replace with Regular Expression

Search with regex, and use `(.*)` to extract the string to be used by replace.
Then use the regex group result with variable name like `$1`, `$2`, `$3`...

For example, if you want to replace following

```
abc/test0
abc/test1
abc/test2
```

with

```
test0/test0
test1/test1
test2/test2
```

Then search with

```
abc/(.*)
```

Replace with

```
$1/$1
```
