# LLDB - Basic Operations

**Print object**

```
(lldb) po responseObject
```

**Print primitive type**

```
(lldb) p some_primitive_value
```

**Resume excution**

```
(lldb) continue
```

**Step over**

```
(lldb) n
```

**Step in**

```
(lldb) s
```

**Print backtrace**

```
(lldb) bt
```