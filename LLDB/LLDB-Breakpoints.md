# LLDB - Breakpoints

**List breakpoints**, it's important that breakpoints operations like `delete`, `enable` and `disable` needs to use the <breakpoint_number> provided by this function. 

```
(lldb) br l
```

**Add a breakpoint** by providing file name and line number.

```
(lldb) b MyViewController.m:30
```

**Add a symbolic breakpoint** by providing method name.

```
(lldb) b method_name
```

**Delete a breakpoint**

```
(lldb) br delete <breakpoint_number>
```

**Enable breakpoint**

```
(lldb) br e <breakpoint_number>
```

**Disable breakpoint**

```
(lldb) br di <breakpoint_number>
```

**Add condition to break point**

```
(lldb) br mod -c "totalValue > 1000" <breakpoint_number>
```

**Reset the condition**

```
(lldb) br mod -c "" <breakpoint_number>
```

**Add debugger command to a break point**

```
(lldb) br com add <breakpoint_number>
```